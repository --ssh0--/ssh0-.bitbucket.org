#! /usr/bin/env python
# -*- coding:utf-8 -*-
#
# written by Shotaro Fujimoto, May 2014.
""" This program is the simuration of random walk in two-dimentional space.
"""
import numpy as np
import matplotlib.pyplot as plt
import sys
from MyDialog import Dialog

def random_walk_d2(l=1, x0=0, y0=0):
    """ Random walk in two-dimentional space.
    """
    global x, y
    x = np.zeros([nwalkers, max(N)], 'i')
    y = np.zeros([nwalkers, max(N)], 'i')
    p = np.random.random([nwalkers, max(N)-1])

    for n in range(nwalkers):
        x[n][0] = x0
        y[n][0] = y0
        for i in range(1,max(N)):
            if p[n][i-1] < 0.25:
                x[n][i] = x[n][i-1] + l
                y[n][i] = y[n][i-1]
            elif p[n][i-1] < 0.5:
                x[n][i] = x[n][i-1] - l
                y[n][i] = y[n][i-1]
            elif p[n][i-1] < 0.75:
                x[n][i] = x[n][i-1] 
                y[n][i] = y[n][i-1] + l
            else:
                x[n][i] = x[n][i-1]
                y[n][i] = y[n][i-1] - l

def calc():
    """ Caluculate the average and the variance of x(N) and y(N)
    """
    x_ave = np.sum(x, axis=0, dtype=np.float32)/nwalkers*1.
    y_ave = np.sum(y, axis=0, dtype=np.float32)/nwalkers*1.
    x_2_ave = np.sum(x**2, axis=0, dtype=np.float32)/nwalkers*1.
    y_2_ave = np.sum(y**2, axis=0, dtype=np.float32)/nwalkers*1.
    variance_x = x_2_ave - x_ave**2
    variance_y = y_2_ave - y_ave**2
    R_2 = x_2_ave + y_2_ave - x_ave**2 - y_ave**2
    
    return [x_ave, y_ave, variance_x, variance_y, R_2]

def draw_figure():
    """ Draw the figure of two-dimentional random walk.
    """
    fig = plt.figure('random walk figure')
    ax = fig.add_subplot(111, aspect='equal')
    ax.grid()
    xmin, xmax = np.amin(x), np.amax(x)
    ymin, ymax = np.amin(y), np.amax(y)
    xmargin, ymargin = (xmax-xmin)*0.05, (ymax-ymin)*0.05
    ax.set_xlim(xmin-xmargin, xmax+xmargin)
    ax.set_ylim(ymin-ymargin, ymax+ymargin)
    ax.set_xlabel(r'$x$')
    ax.set_ylabel(r'$y$')
    
    for n in range(nwalkers):
        l, = ax.plot([],[],'r-')
        l.set_data(x[n], y[n])
    plt.show()

def plot_graph():
    """ Show the graph about the average and the variance of x(N) and y(N).
    """
    fig = plt.figure('random walk graph',figsize=(9,8))

    ax1 = fig.add_subplot(321)
    ax2 = fig.add_subplot(322)
    ax3 = fig.add_subplot(323)
    ax4 = fig.add_subplot(324)
    ax5 = fig.add_subplot(325)
    
    axes = [ax1, ax2, ax3, ax4, ax5]
    data = calc()
    labels = [r'$<x(N)>$', r'$<y(N)>$', r'$<\Delta x^{2}(N)>$', \
              r'$<\Delta y^{2}(N)>$', r'$<\Delta R^{2}(N)>$']
    
    for i, ax in enumerate(axes):
        ax.plot(N, data[i])
        ax.set_xlabel(r'$N$')
        ax.set_ylabel(labels[i], fontsize=12)
    
    fig.subplots_adjust(wspace=0.2,hspace=0.5)
    fig.tight_layout()   
    plt.show()

if __name__ == '__main__':

    N = range(1,501) # caluculate when N = *
    nwalkers = 500 # number of random walkers
    
    def show_figure():
        random_walk_d2()
        draw_figure()
    
    def graph():
        random_walk_d2()
        plot_graph()
    
    window = Dialog()
    window.show_window(["Simulation of random walk in two-dimentional space.",\
                        "Press the button to start the simulation."],\
                       [{'figure':show_figure}, {'graph':graph}, {'Quit':sys.exit}]
                      )

